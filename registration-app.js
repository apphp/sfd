var validationApp = angular.module('validationApp', [])

validationApp.controller('ValidationController', ['$scope', '$window', '$sce', '$location', '$anchorScroll', '$http', function($scope, $window, $sce, $location, $anchorScroll, $http){

  $scope.showValidation = false;
  $scope.warningMessage = false;
  $scope.countryslist = [];
  $scope.newUser = {
            name: '',
            lastname: '',
            email: '',
            pass:'',
            confpass:'',
            address1:'',
            address2:'',
            city:'',
            post:'',
            country:'',
            phone:'',
            agreed: '',
            opt:''
          };

  $scope.regExp = {
        email: /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/
      };
  
  $http.get('data/countries.json').success(function(data) {
        $scope.countryslist = data;
  });

// tto
// A simple method to select country.
// The better solution to use server response.

  $http.get("http://ipinfo.io").success(function(data) {
    $scope.newUser.country = data.country;
  }).error(function(data){
    $scope.newUser.country ="PL";
  });
  
  $scope.$watch('validForm.$valid', function(newVal) {
      $scope.valid = newVal;
  });
  
  $scope.addUser = function (userDetails) {
    if ($scope.valid) {
      $scope.warningMessage = false;
      $scope.message = "Form sent";
      alert($scope.message);
      console.log($scope.newUser);
      // $window.location.reload();
    } else {
      $scope.showValidation = true;
      $scope.warningMessage = true;
    }
  }

  $scope.getError = function (error) {
    if (angular.isDefined(error)) {
      if (error.required) {
        return $sce.trustAsHtml('<p class="bg-danger arrow">Error message</p>');
      } else if (error.pattern) {
        return $sce.trustAsHtml('<p class="bg-danger arrow">Please enter a valid e-mail address</p>');
      }else if (error.mismatch) {
        return $sce.trustAsHtml('<p class="bg-danger arrow">Password and Confirm Password must match</p>');
      }
    }
  }

  $scope.gotoTop = function() {
    $location.hash('top');
    $anchorScroll();
  };
}]);

validationApp.directive('match', function($parse) {
  return {
    require: 'ngModel',
    link: function(scope, elem, attrs, ctrl) {
      scope.$watch(function() {
        return $parse(attrs.match)(scope) === ctrl.$modelValue;
      }, function(currentValue) {
        ctrl.$setValidity('mismatch', currentValue);
      });
    }
  };
});

